<?php

class views_handler_filter_taxonomy_term_or_other_in_operator extends views_handler_filter_in_operator {

  function option_definition() {
    $options = parent::option_definition();
    $options['show_other'] = array('default' => FALSE, 'bool' => TRUE);
    $options['other_label'] = array('default' => '');
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['show_other'] = array(
      '#title' => t('Show "Other" option'),
      '#description' => t('Append the "Other" option to the list of available terms.'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['show_other']),
    );

    $form['other_label'] = array(
      '#type' => 'textfield',
      '#title' => t('"Other" label'),
      '#default_value' => $this->options['other_label'],
      '#description' => t('Optionally use another label than "Other".'),
      '#dependency' => array('edit-options-show-other' => array(TRUE)),
    );
  }

  function get_value_options() {
    if (isset($this->value_options)) {
      return;
    }
    parent::get_value_options();

    if (!empty($this->options['show_other'])) {
      $this->value_options[0] = empty($this->options['other_label']) ? t('Other') : filter_xss_admin($this->options['other_label']);
    }

    return $this->value_options;
  }

}
