<?php
/**
 * Abstract base class defining autocomplete sources.
 */
abstract class TermOrOtherTaxonomyAutocompleteSource {

  protected $settings = array();

  public function __construct(array $settings) {
    $settings += $this->defaults();
    $this->settings = $settings;
  }

  /**
   * Default options for the plugin.
   */
  protected function defaults() {
    return array();
  }

  /**
   * Provide FAPI elements to configure the plugin.
   */
  public function form() {
    return array();
  }

  /**
   * Provide the autocomplete path.
   */
  abstract public function path();

}
