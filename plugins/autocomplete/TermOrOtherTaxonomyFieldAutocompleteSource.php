<?php
/**
 * Defines the Taxonomy Field Autocomplete Sources plugin..
 */
class TermOrOtherTaxonomyFieldAutocompleteSource extends TermOrOtherTaxonomyAutocompleteSource {

  /**
   * Default options for the plugin.
   */
  public function defaults() {
    return array('field_name' => '');
  }

  /**
   * Provide FAPI elements to configure the plugin.
   */
  public function form() {
    $fields = array();
    foreach (field_info_field_map() as $field_name => $field_info) {
      if ($field_info['type'] == 'taxonomy_term_reference' && !empty($field_info['bundles'])) {
        foreach ($field_info['bundles'] as $entity_type => $bundles) {
          foreach ($bundles as $bundle_name) {
            if ($instance = field_info_instance($entity_type, $field_name, $bundle_name)) {
              $fields[$field_name] = check_plain($instance['label']);
              break 2;
            }
          }
        }
      }
    }
    return array(
      'field_name' => array(
        '#type' => 'select',
        '#title' => t('Autocomplete Taxonomy Field Source'),
        '#options' => $fields,
        '#default_value' => $this->settings['field_name'],
      ),
    );
  }


  /**
   * Provide the autocomplete path.
   */
  public function path() {
    if (!empty($this->settings['field_name']) && user_access('access content')) {
      if (($field = field_info_field($this->settings['field_name'])) && $field['type'] == 'taxonomy_term_reference') {
        return 'taxonomy/autocomplete/' . $this->settings['field_name'];
      }
    }
  }
}
