<?php

/**
 * @file
 * Documents API functions for Taxonomy term (with other support) module.
 */

/**
 * Return information about term autocomplete sources.
 *
 * @return
 *   An associative array whose keys are class names of autocomplete sources
 *   and whose values are describing each plugin. Each class name key should
 *   be autoloaded by Drupal (defined as a file include in your modules info
 *   file) and values are an associative array containing:
 *   - title: The human-readable name of the plugin.
 *
 * Defined classes must extend the TermOrOtherTaxonomyAutocompleteSource class.
 *
 * @see TermOrOtherTaxonomyFieldAutocompleteSource
 */
function hook_taxonomy_term_or_other_autocomplete_sources() {
  $items = array(
    'TermOrOtherTaxonomyFieldAutocompleteSource' => array(
      'title' => t('Taxonomy Field'),
    ),
  );

  return $items;
}
